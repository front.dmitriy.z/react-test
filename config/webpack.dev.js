const defaultConfig = require('./webpack.common.config');
const configExtender = require("./config-extender");

const config = configExtender
    .setConfig(defaultConfig)
    .addReactRule()
    .extend();

module.exports = config;