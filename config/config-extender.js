class ConfigExtender {
    setConfig(config) {
        this._config = config;
        return this;
    }

    addExternalEntries(entries) {
        this._config.entry.unshift(...entries);
        return this;
    }

    replaceEntries(entries) {
        if (entries) {
            this._config.entry = Object.assign(this._config.entry, entries);
        }

        return this;
    }

    addReactRule() {
        this._config.module.rules.push({
            test: /\.(js|jsx)$/,
            loader: 'babel-loader',
            options: {
                presets: [
                    '@babel/preset-env',
                    '@babel/preset-react',
                ],
            },
        });

        return this;
    }

    extend() {
        return this._config;
    }
}

module.exports = new ConfigExtender();