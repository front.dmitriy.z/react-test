import React, { Component } from 'react';
import logo from '../../images/logo.svg';
// import '../../styles/App.scss';

class App extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <button type="button" className="main-header__action-btn main-header__action-btn--join">
                        Join for Free
                    </button>
                    <span className="main-header__nav-link-icon fa fa-ladies"></span>
                    <p>
                        Edit <code>src/App.js</code> and save to reload.
                    </p>
                    <a className="App-link"
                        href="https://reactjs.org"
                        target="_blank"
                        rel="noopener noreferrer">
                        Learn React
                    </a>
                </header>
            </div>
        );
    }
}

export default App;
