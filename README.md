## Для работы с подключаемым React модулем необходимо:
### Setup:
1. Установить ***npm*** пакет
2. Подключить репозиторий как ***submodule***.

В директории ***./src*** создать entry файлы для подключения файлов сабмодуля. ( Для js и scss )   
Должно быть 2 файла. Для ***development*** и ***production*** версий. (```<filename>.<ext>```, ```<filename>.build.<ext>```)

**Для JS:**   
В ```<filename>.js``` прокидывается entry из сабмодуля.
В ```<filename>.build.js``` прокидывается entry из npm пакета.

**Для SCSS:**   
В ```<filename>.scss``` прокидывается entry из сабмодуля.
В ```<filename>.build.scss``` прокидывается entry из npm пакета.   
Так же добавляем рядом файл ```<filename>.shared.scss``` для импорта ***scss variables*** текущего проекта.   
И подключаем его в файлы ```<filename>.scss```, ```<filename>.build.scss```.

В нужном **twig** шаблоне вставляем ссылки на файлы.   
Имя линнки должно соответствовать пути ***development entry*** файла относительно директории ***./src***.   
Например:    
```<link rel="stylesheet" href="{{ asset('css/<filename>.scss') }}" type="text/css">```,   
```<script type="text/javascript" src="{{ asset('js/<filename>.js') }}"></script>```

В ***webpack.config.js*** нужно подключить ***configExtender.js*** из npm пакета.
И расширить существующий ***webpack config***.
```javascript
module.exports = (env, options) => {
    process.env.NODE_ENV = options.mode;
    const twigCompilatorWebpackConfig = TwigCompilator.getWebpackConfig();
    const config = configExtender
        .setConfig(twigCompilatorWebpackConfig)
        .replaceEntries(entriesForReplaceByCondition())
        .addReactRule()
        .extend();

    return config;
};

function entriesForReplaceByCondition() {
    if (process.env.NODE_ENV !== 'production') return false;
    return {
        'css/react-app-module': './src/css/react-app-module-build.scss',
        'js/react-app-module': './src/js/react-app-module-build.js'
    }
}
```
**.replaceEntries(entriesForReplaceByCondition())** перезаписывает пути ***development entry*** на ***production entry***.   
.replaceEntries принемает объект вида { <путь как в twig шаблоне> : <путь до production entry файла> }

### Development    
Обновить npm пакет. ***npm install <module>***   
Обновить submodule. ***git submodule update --remote***   
Разработка модуля ведется в сабмодуле. Коммиты изменений делаются из сабмодуля.   

### Production   
Обновить npm пакет. ***npm install <module>***   
Запустить билд.   